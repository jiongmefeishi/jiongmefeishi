<h1 align="center">👋 你好呀，欢迎来到囧么肥事！🤝 </h1>


<h1 align="center"><font color=#ff9900 size=5>2022年最新春招高频面试题，入大厂基础知识</font></h1>
<p align="center">
<a href="https://mp.weixin.qq.com/mp/appmsgalbum?__biz=Mzg3NjU0NDE4NQ==&action=getalbum&album_id=2218140423993212933#wechat_redirect">
<img src="https://img.shields.io/badge/公众号-囧么肥事-green.svg" alt="公众号"></a>
<a href="https://blog.51cto.com/jiongmefeishi">
<img src="https://img.shields.io/badge/51CTO-囧么肥事-informational.svg" alt="51CTO博客"></a>
<a href="https://mp.weixin.qq.com/mp/appmsgalbum?__biz=Mzg3NjU0NDE4NQ==&action=getalbum&album_id=2289253233237737475#wechat_redirect">
<img src="https://img.shields.io/badge/MySQL 经典面试对白案例-囧么肥事-import.svg" alt="MySQL"></a>
<a href="https://mp.weixin.qq.com/mp/appmsgalbum?__biz=Mzg3NjU0NDE4NQ==&action=getalbum&album_id=2240277491397476361#wechat_redirect">
<img src="https://img.shields.io/badge/Kubernetes 经典面试对白案例-囧么肥事-lightgrey.svg" alt="Kubernetes"></a>
<a href="https://gitee.com/jiongmefeishi/JMFS-Programmer-Books">
<img src="https://img.shields.io/badge/囧么肥事-图书馆-orange.svg" alt="囧么肥事图书馆"></a>

<p align="center">

-----



<h3 align="center">这里收集整理最近两年大厂多篇真实面经，整理出一批高频面试题</h3>
<h4 align="center">其中包括腾讯，阿里，字节跳动，百度，京东，美团等一线大厂。</h4>

<h5 align="center">（<a href="./面试真题分类汇总/数据统计来源说明.md" target="_blank">数据统计来源说明</a>）</h5>


----

<h4 align="center">2022年最新春招高频面试题，入大厂基础知识</h4>

<h6 align="center">⚡ <a href="./22年春招金三银四常考面试题搜集/22年春招3月到4月常考算法笔试题.md" target="_blank">22年春招3月到4月常考算法笔试题</a></h6>

<h6 align="center">⚡ <a href="./22年春招金三银四常考面试题搜集/22年春招3月到4月常考Java面试题.md" target="_blank">22年春招3月到4月常考Java面试题</a></h6>

<h6 align="center">⚡ <a href="./22年春招金三银四常考面试题搜集/22年春招3月到4月常考数据库和Redis.md" target="_blank">22年春招3月到4月常考数据库和Redis</a></h6>

<h6 align="center">⚡ <a href="./22年春招金三银四常考面试题搜集/22年春招3月到4月常考网络协议.md" target="_blank">22年春招3月到4月常考网络协议</a></h6>

<h6 align="center">⚡ <a href="./22年春招金三银四常考面试题搜集/22年春招3月到4月常考操作系统.md" target="_blank">22年春招3月到4月常考操作系统</a></h6>

<h6 align="center">⚡ <a href="./22年春招金三银四常考面试题搜集/22年春招3月到4月常考系统设计.md" target="_blank">22年春招3月到4月常考系统设计</a></h6>

<h6 align="center">⚡ <a href="https://gitee.com/jiongmefeishi/JMFS-Interview-Notebook-MySQL" target="_blank">【MySQL面试小抄】</a></h6>

<h6 align="center">⚡ <a href="https://gitee.com/jiongmefeishi/JMFS-Interview-Notebook-Kubernetes" target="_blank">【Kubernetes 面试小抄】</a></h6>


----

<h4 align="center">按收录题型体系查看</h4>

<h6 align="center">⚡ <a href="./面试真题分类汇总/Java.md" target="_blank">Java</a></h6>
<h6 align="center">⚡ <a href="./面试真题分类汇总/MQ.md" target="_blank">MQ</a></h6>
<h6 align="center">⚡ <a href="./面试真题分类汇总/Redis.md" target="_blank">Redis</a></h6>
<h6 align="center">⚡ <a href="./面试真题分类汇总/笔试算法.md" target="_blank">笔试算法</a></h6>
<h6 align="center">⚡ <a href="./面试真题分类汇总/操作系统.md" target="_blank">操作系统</a></h6>
<h6 align="center">⚡ <a href="./面试真题分类汇总/JVM调优.md" target="_blank">JVM调优</a></h6>
<h6 align="center">⚡ <a href="./面试真题分类汇总/Spring全家桶.md" target="_blank">Spring全家桶</a></h6>
<h6 align="center">⚡ <a href="./面试真题分类汇总/SpringBoot.md" target="_blank">Spring Boot</a></h6>
<h6 align="center">⚡ <a href="./面试真题分类汇总/分布式.md" target="_blank">分布式</a></h6>
<h6 align="center">⚡ <a href="./面试真题分类汇总/高并发.md" target="_blank">高并发</a></h6>
<h6 align="center">⚡ <a href="./面试真题分类汇总/集合.md" target="_blank">集合</a></h6>
<h6 align="center">⚡ <a href="./面试真题分类汇总/设计模式.md" target="_blank">设计模式</a></h6>
<h6 align="center">⚡ <a href="./面试真题分类汇总/数据库.md" target="_blank">数据库</a></h6>
<h6 align="center">⚡ <a href="./面试真题分类汇总/网络协议.md" target="_blank">网络协议</a></h6>
<h6 align="center">⚡ <a href="./面试真题分类汇总/系统设计场景题.md" target="_blank">系统设计场景题</a></h6>


<h4 align="center">按收录大厂查看</h4>

<h6 align="center">⚡ <a href="./大厂高频面试真题/阿里/目录.md" target="_blank">阿里</a></h6>
<h6 align="center">⚡ <a href="./大厂高频面试真题/百度/目录.md" target="_blank">百度</a></h6>
<h6 align="center">⚡ <a href="./大厂高频面试真题/京东/目录.md" target="_blank">京东</a></h6>
<h6 align="center">⚡ <a href="./大厂高频面试真题/美团/目录.md" target="_blank">美团</a></h6>
<h6 align="center">⚡ <a href="./大厂高频面试真题/腾讯/目录.md" target="_blank">腾讯</a></h6>
<h6 align="center">⚡ <a href="./大厂高频面试真题/字节跳动/目录.md" target="_blank">字节跳动</a></h6>


---

<h6 align="center"> 📫 联系我       jiongmefeishi@163.com </h6>
<h6 align="center"> 🤝 </h6>

